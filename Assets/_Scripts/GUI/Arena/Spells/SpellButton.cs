﻿using UnityEngine;
using UnityEngine.UI;

public class SpellButton : MonoBehaviour
{
    private Image _image;

    private void Start()
    {
        _image = transform.FindChild("CooldownImage").GetComponent<Image>();
    }

    public void SetCooldown(float percenteges)
    {
        _image.fillAmount = percenteges;
    }
}