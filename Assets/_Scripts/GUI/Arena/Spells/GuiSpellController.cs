﻿using SharedData.Enums;
using UnityEngine;

public class GuiSpellController : MonoBehaviour
{
    public GameObject FireballImage;
    public GameObject TeleportImage;

    public void SetCooldown(SpellName spellName, float cooldown)
    {
        switch (spellName)
        {
            case SpellName.Fireball:
                FireballImage.SendMessage("SetCooldown", cooldown);
                break;

            case SpellName.Teleport:
                TeleportImage.SendMessage("SetCooldown", cooldown);
                break;
        }
    }
}