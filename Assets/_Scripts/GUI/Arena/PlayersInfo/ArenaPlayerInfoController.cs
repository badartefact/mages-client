﻿using UnityEngine;
using UnityEngine.UI;

public class ArenaPlayerInfoController : MonoBehaviour
{
    public string Id { get; set; }

    public string PlayerName
    {
        get { return _playerNameText.text; }
        set { _playerNameText.text = value; }
    }

    public string Damage
    {
        get { return _playerDamageText.text; }
        set { _playerDamageText.text = value; }
    }

    public Color PanelColor
    {
        get { return _image.color; }
        set { _image.color = value; }
    }

    private Text _playerNameText;
    private Text _playerDamageText;
    private Image _image;

    private void Awake()
    {
        _playerNameText = transform.FindChild("PlayerName").GetComponent<Text>();
        _playerDamageText = transform.FindChild("PlayerDamage").GetComponent<Text>();
        _image = GetComponent<Image>();
    }
}