﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ArenaPlayerInfoPanelController : MonoBehaviour
{
    public GameObject PlayerInfoPrefab;

    private Dictionary<string, ArenaPlayerInfoController> _playerInfoList;

    private void Awake()
    {
        _playerInfoList = new Dictionary<string, ArenaPlayerInfoController>();
        HidePanel();
    }

    private void HidePanel()
    {
        gameObject.SetActive(false);
    }

    private void ShowPanel()
    {
        gameObject.SetActive(true);
    }

    public void Set(string id, string playerName, float damage)
    {
        if (_playerInfoList.ContainsKey(id))
        {
            UpdateItem(id, damage);
        }
        else
        {
            AddItem(id, playerName, damage);
            ShowPanel();
        }
    }

    private void AddItem(string id, string playerName, float damage)
    {
        Debug.Log(string.Format("Created item for: id - '{0}', name - '{1}', damage - '{2}'", id, playerName, damage));
        var playerInfoGameObject = Instantiate(PlayerInfoPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        var arenaPlayerInfoController = playerInfoGameObject.GetComponent<ArenaPlayerInfoController>();
        arenaPlayerInfoController.Id = id;
        arenaPlayerInfoController.PlayerName = playerName;
        arenaPlayerInfoController.Damage = Convert.ToString(damage);
        _playerInfoList.Add(id, arenaPlayerInfoController);

        playerInfoGameObject.transform.SetParent(transform);
    }

    private void UpdateItem(string id, float damage)
    {
        var item = _playerInfoList[id];
        item.Damage = Convert.ToString(damage);
    }

    public void UpdateOrder()
    {
        var gameObjectsAndDamages = _playerInfoList.Values.Select(p => new KeyValuePair<Transform, float>(p.transform, float.Parse(p.Damage))).ToList();
        gameObjectsAndDamages.Sort((firstPair, nextPair) => firstPair.Value.CompareTo(nextPair.Value));
        var array = gameObjectsAndDamages.Select(g => g.Key).ToArray();
        var k = array.Length - 1;
        foreach (var item in array)
        {
            item.SetSiblingIndex(k);
            k--;
        }
    }
}