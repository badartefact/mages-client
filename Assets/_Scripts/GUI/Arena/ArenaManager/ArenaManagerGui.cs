﻿using SharedData.Types;
using System;
using System.Collections.Generic;
using UnityEngine;

public class ArenaManagerGui : MonoBehaviourThread
{
    public GameObject GameListPrefab;
    public GameObject GamePanelPrefab;

    private List<ArenaGameData> _games;
    private ArenaGameData _currentGame;
    private GamePanelController _gamePanelController;
    private GameListController _gameListController;

    private InternetManager _internetManager;

    private GameObject _gamePanelObject;
    private GameObject _gameListObject;
    private Transform _canvas;

    private void Awake()
    {
        _games = new List<ArenaGameData>();

        _internetManager = GameObject.Find("InternetManager").GetComponent<InternetManager>();
        _canvas = GameObject.Find("Canvas").transform;

        CreateGameListObject();
        CreateGamePanelObject();

        _gamePanelController = _gamePanelObject.GetComponent<GamePanelController>();
        _gameListController = _gameListObject.GetComponent<GameListController>();
    }

    private void Start()
    {
        _internetManager.RequestAvailableGames();
    }

    private void OnEnable()
    {
        _internetManager.OnNewGamesAvailable += OnNewGamesReceived;
        _internetManager.OnGameUpdate += OnUpdateGame;
        _internetManager.OnGameStart += OnGameStart;
        _internetManager.OnDisconnectFromLobby += OnDisconnectFromLobby;

        _gamePanelController.OnReady += GamePanelControllerOnReady;
        _gamePanelController.OnDisconnect += GamePanelControllerOnDisconnect;

        _gameListController.OnConnectToGame += OnConnectToGame;
    }

    private void OnDisable()
    {
        _internetManager.OnNewGamesAvailable -= OnNewGamesReceived;
        _internetManager.OnGameUpdate -= OnUpdateGame;
        _internetManager.OnGameStart -= OnGameStart;
        _internetManager.OnDisconnectFromLobby -= OnDisconnectFromLobby;

        _gamePanelController.OnReady -= GamePanelControllerOnReady;
        _gamePanelController.OnDisconnect -= GamePanelControllerOnDisconnect;

        _gameListController.OnConnectToGame -= OnConnectToGame;
    }

    private void CreateGamePanelObject()
    {
        _gamePanelObject = Instantiate(GamePanelPrefab) as GameObject;
        _gamePanelObject.transform.SetParent(_canvas, false);
        _gamePanelObject.SetActive(false);
    }

    private void CreateGameListObject()
    {
        _gameListObject = Instantiate(GameListPrefab) as GameObject;
        _gameListObject.transform.SetParent(_canvas, false);
    }

    private void OnDisconnectFromLobby()
    {
        UpdateGuiAfterDisconnectFromLobby();

        _internetManager.RequestAvailableGames();
    }

    private void OnGameStart(string code, string ip, int port)
    {
        SceneSettings.Code = code;
        SceneSettings.Ip = ip;
        SceneSettings.Port = port;

        Debug.Log("Going to load level.");

        RunInMainThread(() => Application.LoadLevel(1));
    }

    private void OnUpdateGame(ArenaGameData game)
    {
        _currentGame = game;

        RunInMainThread(() =>
        {
            _gameListObject.SetActive(false);
            ActivateGamePanel();
            UpdateGamePlayersPanel();
        });
    }

    private void ActivateGamePanel()
    {
        _gamePanelObject.SetActive(true);
    }

    private void GamePanelControllerOnDisconnect()
    {
        _internetManager.DisconnectFromGame();
        _internetManager.RequestAvailableGames();

        UpdateGuiAfterDisconnectFromLobby();
    }

    private void UpdateGuiAfterDisconnectFromLobby()
    {
        _gamePanelObject.SetActive(false);
        _gameListObject.SetActive(true);
    }

    private void GamePanelControllerOnReady()
    {
        _internetManager.SendReady();
    }

    private void UpdateGamePlayersPanel()
    {
        _gamePanelController.Clear();

        foreach (var playerData in _currentGame.Players)
        {
            _gamePanelController.AddPlayer(playerData.Name, playerData.IsReady);
        }
    }

    private void OnNewGamesReceived(List<ArenaGameData> games)
    {
        Debug.Log("ArenaManagerGui: new games received.");
        _games = games;
        RunInMainThread(UpdateGameList);
    }

    private void UpdateGameList()
    {
        _gameListController.UpdateGames(_games);
    }

    private void OnConnectToGame(Guid id)
    {
        _internetManager.ConnectToGame(id);
    }
}