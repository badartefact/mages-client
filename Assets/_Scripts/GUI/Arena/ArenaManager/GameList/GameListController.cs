﻿using SharedData.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameListController : MonoBehaviour
{
    public GameObject GameItemPrefab;

    public event Action<Guid> OnConnectToGame = delegate { };

    private List<GameListItemController> _gameItemControllers;
    private List<ArenaGameData> _games;

    private void Awake()
    {
        _gameItemControllers = new List<GameListItemController>();
    }

    private void AddGameListItem(Guid id, string name, int currentAmount, int maxAmount)
    {
        var item = (GameObject)Instantiate(GameItemPrefab, GameItemPrefab.transform.position, GameItemPrefab.transform.rotation);
        var controller = item.GetComponent<GameListItemController>();
        controller.GameName = name;
        controller.MaxAmount = maxAmount;
        controller.Id = id;
        controller.CurrentAmount = currentAmount;
        controller.OnConnect += OnConnectToGameRequest;

        item.transform.SetParent(transform, false);

        _gameItemControllers.Add(controller);
    }

    private void OnConnectToGameRequest(Guid gameId)
    {
        OnConnectToGame(gameId);
    }

    public void UpdateGames(List<ArenaGameData> games)
    {
        _games = games;
        DeleteNonExistingGames();
        UpdateExistingItems();
        AddNewGames();
    }

    private void AddNewGames()
    {
        Debug.Log("Starting adding new games.");
        var serverGuids = _games.Select(g => g.Id);
        var currentGuids = _gameItemControllers.Select(g => g.Id);

        var newGamesGuids = serverGuids.Except(currentGuids);

        var newGames = _games.Where(g => newGamesGuids.Contains(g.Id)).ToArray();

        foreach (var arenaGameData in newGames)
        {
            Debug.Log(string.Format("Adding game. Id: '{0}', Name: '{1}', CurrentAmount: '{2}', MaximumAmount: '{3}'.",
                arenaGameData.Id, arenaGameData.Name, arenaGameData.CurrentAmount, arenaGameData.MaximumAmount));
            AddGameListItem(arenaGameData.Id, arenaGameData.Name, arenaGameData.CurrentAmount, arenaGameData.MaximumAmount);
        }
        Debug.Log("Stopped adding new games.");
    }

    private void UpdateExistingItems()
    {
        Debug.Log("Starting updating existing items.");
        var serverGuids = _games.Select(g => g.Id);
        var currentGuids = _gameItemControllers.Select(g => g.Id);

        var existingItems = currentGuids.Intersect(serverGuids).ToList();

        foreach (var existingItemGuid in existingItems)
        {
            var controller = _gameItemControllers.FirstOrDefault(g => g.Id == existingItemGuid);
            if (controller != null)
            {
                var newData = _games.FirstOrDefault(g => g.Id == existingItemGuid);
                Debug.Log(string.Format("Property: {0}. Old value: '{1}', new value: '{2}'.", "GameName", controller.GameName, newData.Name));
                controller.GameName = newData.Name;
                Debug.Log(string.Format("Property: {0}. Old value: '{1}', new value: '{2}'.", "CurrentAmount", controller.CurrentAmount, newData.CurrentAmount));
                controller.CurrentAmount = newData.CurrentAmount;
                Debug.Log(string.Format("Property: {0}. Old value: '{1}', new value: '{2}'.", "MaxAmount", controller.MaxAmount, newData.MaximumAmount));
                controller.MaxAmount = newData.MaximumAmount;
            }
        }
        Debug.Log("Stopped updating existing items.");
    }

    private void DeleteNonExistingGames()
    {
        Debug.Log("Starting deleting non existing games.");
        var serverGuids = _games.Select(g => g.Id);
        var currentGuids = _gameItemControllers.Select(g => g.Id);

        var deletedItemsGuids = currentGuids.Except(serverGuids).ToList();
        foreach (var deletedItemGuid in deletedItemsGuids)
        {
            var item = _gameItemControllers.FirstOrDefault(g => g.Id == deletedItemGuid);
            if (item != null)
            {
                Debug.Log("Deleting game: " + item.GameName);
                item.OnConnect -= OnConnectToGameRequest;
                Destroy(item.gameObject);
                _gameItemControllers.Remove(item);
            }
        }
        Debug.Log("Stopped deleting non existing games.");
    }
}