﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameListItemController : MonoBehaviour
{
    public GameObject GameNameObject;
    public GameObject PlayersCountObject;
    public GameObject ConnectButtonObject;

    public event Action<Guid> OnConnect = delegate { };

    public string GameName
    {
        get
        {
            return _gameName.text;
        }
        set
        {
            _gameName.text = value;
        }
    }

    public int CurrentAmount
    {
        get
        {
            return _currentAmount;
        }
        set
        {
            _currentAmount = value;
            UpdatePlayersCount();
        }
    }

    public int MaxAmount
    {
        get
        {
            return _maxAmount;
        }
        set
        {
            _maxAmount = value;
            UpdatePlayersCount();
        }
    }

    public Guid Id { get; set; }

    private Text _gameName;
    private Text _playersCount;
    private Button _connectButton;

    private int _currentAmount;
    private int _maxAmount;

    public void Awake()
    {
        _gameName = GameNameObject.GetComponent<Text>();
        _playersCount = PlayersCountObject.GetComponent<Text>();
        _connectButton = ConnectButtonObject.GetComponent<Button>();
        _connectButton.onClick.AddListener(() => { OnConnect(Id); });

        _currentAmount = 0;
        _maxAmount = 100;
    }

    private void UpdatePlayersCount()
    {
        _playersCount.text = string.Format("{0} / {1}", _currentAmount, _maxAmount);
    }
}