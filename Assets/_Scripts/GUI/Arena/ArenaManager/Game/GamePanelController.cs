﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GamePanelController : MonoBehaviour
{
    public event Action OnReady = delegate { };

    public event Action OnDisconnect = delegate { };

    public GameObject PlayerInfoPanelPrefab;
    public GameObject ReadyButtonObject;
    public GameObject DisconnectButtonObject;

    private Button _readyButton;
    private Button _disconnectButton;
    private Transform _gamePlayersPanelObject;

    private void Awake()
    {
        _gamePlayersPanelObject = transform.FindChild("PlayersList");
        _readyButton = ReadyButtonObject.GetComponent<Button>();
        _disconnectButton = DisconnectButtonObject.GetComponent<Button>();
    }

    private void OnEnable()
    {
        _readyButton.onClick.AddListener(() => OnReady());
        _disconnectButton.onClick.AddListener(() => OnDisconnect());
    }

    private void OnDisable()
    {
        _readyButton.onClick.RemoveAllListeners();
        _disconnectButton.onClick.RemoveAllListeners();
    }

    public void Clear()
    {
        _gamePlayersPanelObject.Cast<Transform>().ToList().ForEach(p => Destroy(p.gameObject));
    }

    public void AddPlayer(string playerName, bool isReady)
    {
        var playerInfoPanel = (GameObject)Instantiate(PlayerInfoPanelPrefab, Vector3.zero, Quaternion.identity);
        var controller = playerInfoPanel.GetComponent<PlayerInfoController>();
        controller.PlayerName = playerName;
        controller.Status = isReady;
        playerInfoPanel.transform.SetParent(_gamePlayersPanelObject, false);
    }
}