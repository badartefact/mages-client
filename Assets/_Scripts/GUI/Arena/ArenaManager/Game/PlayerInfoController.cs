﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerInfoController : MonoBehaviour
{
    public GameObject PlayerNameObject;
    public GameObject StatusObject;

    private Text _playerNameText;
    private Text _statusText;

    private bool _status;

    public string PlayerName
    {
        get
        {
            return _playerNameText.text;
        }
        set
        {
            _playerNameText.text = value;
        }
    }

    public bool Status
    {
        get
        {
            return _status;
        }
        set
        {
            _status = value;
            UpdateStatus();
        }
    }

    private void UpdateStatus()
    {
        if (_status)
        {
            _statusText.text = "Ready";
            GetComponent<Image>().color = Color.green;
        }
        else
        {
            _statusText.text = "Not ready";
            GetComponent<Image>().color = Color.yellow;
        }
    }

    private void Awake()
    {
        _playerNameText = PlayerNameObject.GetComponent<Text>();
        _statusText = StatusObject.GetComponent<Text>();
    }
}