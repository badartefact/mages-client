﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerGuiController : MonoBehaviour
{
    public GameObject HpBarPrefab;

    private GameObject _hpBar;
    private Image _image;
    private float _hitPoints;

    public float HitPoints
    {
        get
        {
            return _hitPoints;
        }
        set
        {
            _hitPoints = value;
            UpdateGui();
        }
    }

    private void UpdateGui()
    {
        _image.fillAmount = _hitPoints / 100;
    }

    private void Awake()
    {
        _hpBar = (GameObject)Instantiate(HpBarPrefab, Vector3.zero, Quaternion.identity);
        _image = _hpBar.transform.FindChild("Hp").GetComponent<Image>();

        _hpBar.transform.rotation = new Quaternion(0.4f, 0, 0, 0.9f);
    }

    private void Update()
    {
        MoveHpBar();
    }

    private void MoveHpBar()
    {
        _hpBar.transform.localPosition = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z);
    }

    private void OnEnable()
    {
        _hpBar.SetActive(true);
    }

    private void OnDisable()
    {
        if (_hpBar != null)
        {
            _hpBar.SetActive(false);
        }
    }
}