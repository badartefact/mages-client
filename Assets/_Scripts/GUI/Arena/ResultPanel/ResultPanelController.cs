﻿using SharedData.Types.FinishResult;
using UnityEngine;
using UnityEngine.UI;

public class ResultPanelController : MonoBehaviourThread
{
    private ResultPlayerListController _playerListController;
    private Button _button;

    private void Awake()
    {
        _button = GetComponentInChildren<Button>();
    }

    private void OnEnable()
    {
        _button.onClick.AddListener(ButtonClicked);
    }

    private void OnDisabled()
    {
        _button.onClick.RemoveListener(ButtonClicked);
    }

    private void ButtonClicked()
    {
        RunInMainThread(() => Application.LoadLevel("ArenaManager"));
    }

    public void Set(FinishData data)
    {
        if (_playerListController == null)
        {
            _playerListController = GetComponentInChildren<ResultPlayerListController>();
        }
        _playerListController.Set(data);
    }
}