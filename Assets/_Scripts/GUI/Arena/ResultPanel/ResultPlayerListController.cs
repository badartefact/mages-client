﻿using SharedData.Types.FinishResult;
using System;
using System.Linq;
using UnityEngine;

public class ResultPlayerListController : MonoBehaviour
{
    public GameObject PlayerInfoPrefab;

    public void Set(FinishData data)
    {
        var sortedPlayersInfo = data.PlayersInfo.ToList();
        sortedPlayersInfo.Sort((firstPair, nextPair) => firstPair.Value.TotalDamage.CompareTo(nextPair.Value.TotalDamage));

        var k = sortedPlayersInfo.Count - 1;
        foreach (var playerFinishInfo in sortedPlayersInfo)
        {
            var playerInfoGameobject = Instantiate(PlayerInfoPrefab) as GameObject;
            playerInfoGameobject.transform.SetParent(transform, false);
            playerInfoGameobject.transform.SetSiblingIndex(k);
            k--;

            var controller = playerInfoGameobject.GetComponent<ArenaPlayerInfoController>();
            controller.Damage = Convert.ToString(playerFinishInfo.Value.TotalDamage);
            controller.PlayerName = playerFinishInfo.Value.Name;
            controller.PanelColor = Color.white;

            if (data.WinnerId == playerFinishInfo.Key)
            {
                controller.PanelColor = Color.green;
            }
        }
    }
}