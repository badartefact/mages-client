﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuGUI : MonoBehaviourThread
{
    public GameObject AuthPanelPrefab;

    private GameObject AuthPanelObject
    {
        get
        {
            if (_authPanelObject == null)
            {
                CreateAuthPanelObject();
            }
            return _authPanelObject;
        }
    }

    private const string ConnectButtonName = "AuthButton";
    private const string UsernameInputFieldName = "UsernameInputField";
    private const string PasswordInputFieldName = "PasswordInputField";

    private Button _connectButton;
    private InputField _usernameInputField;
    private InputField _passwordInputField;

    private InternetManager _internetManager;

    private GameObject _authPanelObject;
    private Transform _canvas;

    private Transform Canvas
    {
        get { return _canvas ?? (_canvas = GameObject.Find("Canvas").transform); }
    }

    private void Awake()
    {
        _internetManager = GameObject.Find("InternetManager").GetComponent<InternetManager>();

        InitGui();
        SetStandartValues();
    }

    private void OnEnable()
    {
        _internetManager.OnConnected += OnConnetedToServer;
        _internetManager.OnConnectError += OnErrorToServer;
    }

    private void OnDisable()
    {
        _internetManager.OnConnected -= OnConnetedToServer;
        _internetManager.OnConnectError -= OnErrorToServer;
    }

    private void CreateAuthPanelObject()
    {
        _authPanelObject = Instantiate(AuthPanelPrefab) as GameObject;
        _authPanelObject.transform.SetParent(Canvas, false);
    }

    private void OnErrorToServer()
    {
        RunInMainThread(SetStandartValues);
        Debug.Log("Cannot connect to play server!");
    }

    private void OnConnetedToServer()
    {
        Debug.Log("Connected to play server!");
        RunInMainThread(() => Application.LoadLevel("ArenaManager"));
    }

    private void InitGui()
    {
        InitButton();
        InitInputField(out _usernameInputField, UsernameInputFieldName);
        InitInputField(out _passwordInputField, PasswordInputFieldName);

        _usernameInputField.text = Application.isEditor ? "editor" : "test";
        _passwordInputField.text = "test";
    }

    private void InitInputField(out InputField inputField, string usernameInputFieldName)
    {
        inputField = null;
        Transform inputFieldTransform = AuthPanelObject.transform.Cast<Transform>().FirstOrDefault(t => t.name == usernameInputFieldName);
        if (inputFieldTransform != null)
        {
            inputField = inputFieldTransform.GetComponent<InputField>();
        }
    }

    private void InitButton()
    {
        Transform buttonTransform = AuthPanelObject.transform.Cast<Transform>().FirstOrDefault(t => t.name == ConnectButtonName);
        if (buttonTransform != null)
        {
            _connectButton = buttonTransform.GetComponent<Button>();
            if (_connectButton != null)
            {
                _connectButton.onClick.AddListener(Connect);
            }
        }
    }

    private void SetStandartValues()
    {
        _connectButton.interactable = true;
        _connectButton.GetComponentInChildren<Text>().text = "Login";
    }

    private void Connect()
    {
        _connectButton.interactable = false;
        _connectButton.GetComponentInChildren<Text>().text = "Loggening in...";
        _internetManager.Login(_usernameInputField.text, _passwordInputField.text);
    }
}