﻿using UnityEngine;

public class GroundController : MonoBehaviour
{
    private volatile float _radius = 30;

    public float Radius
    {
        get
        {
            return _radius;
        }
        set
        {
            _radius = value;
        }
    }

    private void Update()
    {
        //transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(_radius / 5, _radius / 5), Time.deltaTime * 2);
        transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(_radius * 2, transform.localScale.y, _radius * 2), Time.deltaTime * 2);
    }
}