﻿using UnityEngine;

public class HPManager
{
    private float _hitPoints = 100;

    public bool Dead
    {
        get
        {
            return _hitPoints <= 0;
        }
    }

    public float HitPoints
    {
        get
        {
            return _hitPoints;
        }
        set
        {
            _hitPoints = value;
        }
    }

    public void AddHP(float amount)
    {
        _hitPoints += Mathf.Abs(amount);
    }

    public void RemoveHP(float amount)
    {
        _hitPoints -= Mathf.Abs(amount);
    }
}