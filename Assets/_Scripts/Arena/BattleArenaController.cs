using SharedData.Enums;
using SharedData.Exchanging.DataClasses;
using SharedData.Types;
using SharedData.Types.FinishResult;
using SharedData.Types.Snapshot;
using SharedData.Types.Snapshot.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets._Scripts.Arena
{
    public class BattleArenaController : MonoBehaviourThread
    {
        #region Prefabs

        public PrefabsGroup Prefabs;

        #endregion Prefabs

        #region Existing game objects

        public GameObject GroundObject;

        public GameObject ArenaPlayerInfoPanel;

        #endregion Existing game objects

        #region Private memebers

        private List<CharacterEntity> _players;
        private List<GameEntity> _spells;
        private ArenaGameNetworkClient _networkClient;

        private GroundController _groundController;
        private ArenaPlayerInfoPanelController _arenaPlayerInfoPanelController;

        private Canvas _canvas;
        private GuiSpellController _guiSpellController;

        #endregion Private memebers

        #region MonoBehaviour

        private void Awake()
        {
            _players = new List<CharacterEntity>();
            _spells = new List<GameEntity>();

            _groundController = GroundObject.GetComponent<GroundController>();
            _arenaPlayerInfoPanelController = ArenaPlayerInfoPanel.GetComponent<ArenaPlayerInfoPanelController>();
            _guiSpellController = GetComponent<GuiSpellController>();

            InitInternetManager();
        }

        private void Start()
        {
            _canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        }

        protected override void OnUpdate()
        {
            var players = new List<CharacterEntity>(_players);
            foreach (var entity in players)
            {
                entity.Update();
            }

            var spells = new List<GameEntity>(_spells);
            foreach (var entity in spells)
            {
                entity.Update();
            }
        }

        private void OnEnable()
        {
            _networkClient.OnSnapshotUpdate += OnSnapshotUpdate;
            _networkClient.OnFinishDataReceived += NetworkClientOnFinishDataReceived;
        }

        public void OnDisable()
        {
            _networkClient.OnSnapshotUpdate -= OnSnapshotUpdate;
            _networkClient.OnFinishDataReceived -= NetworkClientOnFinishDataReceived;
        }

        public void OnDispose()
        {
            _networkClient.Stop();
        }

        #endregion MonoBehaviour

        #region Events

        private void NetworkClientOnFinishDataReceived(FinishData data)
        {
            Debug.Log("Received finish data. Going to run some methods in main thread.");
            RunInMainThread(
                () =>
                {
                    Debug.Log(string.Format("Received finish data. Winner name: {0},", data.PlayersInfo[data.WinnerId].Name));
                    var finishWindow = Instantiate(Prefabs.EndGameWindowPrefab);
                    finishWindow.transform.SetParent(_canvas.transform, false);
                    var controller = finishWindow.GetComponent<ResultPanelController>();
                    controller.Set(data);
                }
            );
        }

        private void OnSnapshotUpdate(SnapshotData snapshot)
        {
            RunInMainThread(() => ParseSnapshot(snapshot));
        }

        #endregion Events

        #region Public methods

        public void MoveTo(Vector3 target)
        {
            var command = new MoveTo { Position = GetTVector3(target) };
            _networkClient.Send(command);
        }

        public void CastSpell(SpellName spellName, Vector3 target)
        {
            var command = new CastSpell { Target = GetTVector3(target), SpellName = spellName };
            _networkClient.Send(command);
        }

        #endregion Public methods

        #region Private methods

        private void InitInternetManager()
        {
            Debug.Log(string.Format("Going to connect to: '{0}:{1}' with code: '{2}'.", SceneSettings.Ip, SceneSettings.Port, SceneSettings.Code));
            _networkClient = new ArenaGameNetworkClient(SceneSettings.Ip, SceneSettings.Port, SceneSettings.Code);
            _networkClient.Start();
        }

        private void ParseSnapshot(SnapshotData snapshot)
        {
            try
            {
                if (snapshot.ObjectsOnScene != null)
                {
                    foreach (var baseEntity in snapshot.ObjectsOnScene)
                    {
                        switch (baseEntity.Type)
                        {
                            case EntityType.Character:
                                var player = baseEntity as CharacterEntityInfo;
                                if (player != null)
                                {
                                    UpdatePlayerPosition(player);
                                }
                                break;

                            case EntityType.Spell:
                                var spell = baseEntity as SpellEntity;
                                if (spell != null)
                                {
                                    switch (spell.SpellName)
                                    {
                                        case SpellName.Fireball:
                                            UpdateSpellPosition(spell);
                                            break;
                                    }
                                }
                                break;
                        }
                    }

                    UpdateSpellObjectsOnScene(snapshot.ObjectsOnScene);
                    UpdateSpellsCooldown(snapshot.GameData.SpellCooldowns);
                }

                _groundController.Radius = snapshot.GameData.Radius;
            }
            catch (Exception ex)
            {
                Debug.Log("Snapshot error: " + ex);
            }

            _arenaPlayerInfoPanelController.UpdateOrder();
        }

        private void UpdateSpellsCooldown(Dictionary<SpellName, float> spellsCooldown)
        {
            foreach (var spell in spellsCooldown)
            {
                _guiSpellController.SetCooldown(spell.Key, spell.Value);
            }
        }

        private void UpdateSpellObjectsOnScene(IEnumerable<BaseEntity> newSpells)
        {
            var existingSpells = _spells.Select(s => s.Id);
            var spellsFromSnapshot = newSpells.Select(s => s.Id);
            var difference = existingSpells.Except(spellsFromSnapshot);
            foreach (var id in difference)
            {
                RemoveSpellGameobject(id);
            }
        }

        private void UpdateSpellPosition(SpellEntity spell)
        {
            var existingSpell = _spells.FirstOrDefault(p => p.Id == spell.Id);
            var position = new Vector3(spell.Position.X, spell.Position.Y, spell.Position.Z);
            var rotation = new Quaternion(spell.Orientation.X, spell.Orientation.Y, spell.Orientation.Z, spell.Orientation.W);

            if (existingSpell != null)
            {
                existingSpell.Target = position;
                existingSpell.Rotation = rotation;
            }
            else
            {
                GameObject prefab = null;
                if (spell.SpellName == SpellName.Fireball)
                {
                    prefab = Prefabs.FireballPrefab;
                }
                var newSpell = (GameObject)Instantiate(prefab, position, rotation);
                _spells.Add(new CharacterEntity(spell.Id, newSpell));
            }
        }

        private void UpdatePlayerPosition(CharacterEntityInfo player)
        {
            UpdateGui(player);

            var existingPlayer = _players.FirstOrDefault(p => p.Id == player.Id);
            var position = new Vector3(player.Position.X, player.Position.Y, player.Position.Z);
            var rotation = new Quaternion(player.Orientation.X, player.Orientation.Y, player.Orientation.Z, player.Orientation.W);

            if (existingPlayer != null)
            {
                existingPlayer.Target = position;
                existingPlayer.Rotation = rotation;
                existingPlayer.HitPoints = player.HitPoints;
                existingPlayer.IsDead = player.IsDead;
                existingPlayer.TotalDamage = player.Damage;
                existingPlayer.IsRunning = player.IsRunning;
                existingPlayer.IsCasting = player.IsCasting;

                existingPlayer.Entity.GetComponent<PlayerGuiController>().HitPoints = player.HitPoints;

                if (!player.IsConnected || player.IsDead)
                {
                    existingPlayer.Entity.SetActive(false);
                }
                else
                {
                    existingPlayer.Entity.SetActive(true);
                }
            }
            else
            {
                var newPlayer = (GameObject)Instantiate(Prefabs.PlayerPrefab, position, rotation);
                var newEntity = new CharacterEntity(player.Id, newPlayer) { HitPoints = player.HitPoints };
                _players.Add(newEntity);
            }
        }

        private void UpdateGui(CharacterEntityInfo player)
        {
            _arenaPlayerInfoPanelController.Set(player.Id, player.Name, player.Damage);
        }

        private void RemoveSpellGameobject(string id)
        {
            var spellObject = _spells.Where(s => s.Id == id).Select(s => s.Entity).FirstOrDefault();
            if (spellObject != null)
            {
                Destroy(spellObject);
            }
        }

        private TVector3 GetTVector3(Vector3 target)
        {
            return new TVector3 { X = target.x, Y = target.y, Z = target.z };
        }

        #endregion Private methods
    }

    [Serializable]
    public class PrefabsGroup
    {
        public GameObject PlayerPrefab;
        public GameObject FireballPrefab;
        public GameObject EndGameWindowPrefab;
    }
}
