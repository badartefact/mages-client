﻿public enum AIPlayState
{
    Chasing,
    Saving,
    Attacking,
    Avoiding,
    Standing,
    DoingSomeStuff,
    MovingToFirePosition,
    SearchingForEnemies
}