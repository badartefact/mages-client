﻿using SharedData.Enums;
using System;

public static class SpellNameParser
{
    public static SpellName Parse(string name)
    {
        SpellName result = SpellName.None;
        foreach (SpellName spellName in Enum.GetValues(typeof(SpellName)))
        {
            if (spellName.ToString() == name)
            {
                result = spellName;
                break;
            }
        }
        return result;
    }
}