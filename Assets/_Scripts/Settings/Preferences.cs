﻿public static class Preferences
{
    public static string GroundTag = "ground";
    public static string LavaTag = "lava";

    public static float StopDistance = 0.2f;

    public static float ConsumeSpeed = 2.8f;
}