using Newtonsoft.Json;
using SharedData.Configuration;
using SharedData.Enums;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace GameServer.Configuration
{
    public class ConfigurationManager
    {
        private static ConfigurationManager _instance;
        private const string PathToFile = "config";
        private readonly ConfigData _data;
        private readonly Dictionary<SpellName, SpellData> _spellsData;

        public Dictionary<SpellName, SpellData> SpellsDescriptions
        {
            get { return _spellsData; }
        }

        public static ConfigurationManager Instance
        {
            get { return _instance ?? (_instance = new ConfigurationManager()); }
        }

        private ConfigurationManager()
        {
            var data = Resources.Load(PathToFile) as TextAsset;
            _data = JsonConvert.DeserializeObject<ConfigData>(data.text);
            _spellsData = _data.Spells;
        }
    }
}
