﻿public static class GlobalSettings
{
    public static int ActionLayer = 1 << 8;
    public static int GroundLayer = 1 << 9;
    public static int LavaLayer = 1 << 10;

    public static int CameraYVeticalSpeed = 3;
    public static int CameraZVerticalSpeed = 3;

    public static int CameraXHorizontalSpeed = 5;
    public static int CameraZHorizontalSpeed = 5;
}