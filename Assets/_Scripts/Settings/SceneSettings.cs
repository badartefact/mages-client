﻿public class SceneSettings
{
    public static string Ip { get; set; }

    public static int Port { get; set; }

    public static string Code { get; set; }
}