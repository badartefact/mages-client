﻿using UnityEngine;

public class ImpactObject
{
    public Vector3 Impact
    {
        get;
        private set;
    }

    public bool Dead
    {
        get
        {
            return Impact == Vector3.zero;
        }
    }

    public ImpactObject(Vector3 direction)
    {
        Impact = direction;
    }

    public ImpactObject(Vector3 direction, float force)
    {
        direction.y = 0;
        Impact = direction * force;
    }

    public void ConsumeImpact()
    {
        Impact = Vector3.Lerp(Impact, Vector3.zero, Preferences.ConsumeSpeed * Time.deltaTime);
    }
}