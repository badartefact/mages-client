﻿using UnityEngine;

public class CharacterEntity : GameEntity
{
    public float HitPoints { get; set; }

    public bool IsDead { get; set; }

    public float TotalDamage { get; set; }

    public CharacterEntity(string id, GameObject player)
        : base(id, player)
    {
    }
}