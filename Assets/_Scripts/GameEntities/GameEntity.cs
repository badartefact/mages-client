﻿using UnityEngine;

public class GameEntity
{
    protected const float LerpValue = 22.5f;

    public string Id { get; private set; }

    public GameObject Entity { get; set; }

    protected Vector3 _target;

    private readonly Animator _animator;

    public bool IsRunning
    {
        set
        {
            _animator.SetBool("IsRunning", value);
        }
    }

    public bool IsCasting
    {
        set
        {
            _animator.SetBool("IsCasting", value);
        }
    }

    public Vector3 Position
    {
        get
        {
            return Entity.transform.position;
        }
        set
        {
            Entity.transform.position = value;
        }
    }

    public Vector3 Target
    {
        get
        {
            return _target;
        }
        set
        {
            _target = value;
        }
    }

    public Quaternion Rotation
    {
        get
        {
            return Entity.transform.rotation;
        }
        set
        {
            if (Entity != null)
            {
                Entity.transform.rotation = value;
            }
        }
    }

    public GameEntity(string id, GameObject player)
    {
        Id = id;
        Entity = player;
        Target = player.transform.position;
        _animator = player.GetComponent<Animator>();
    }

    public void Update()
    {
        if (Entity != null)
        {
            var distance = Vector3.Distance(Position, Target);
            if (distance > 0.5f)
            {
                Position = _target;
            }
            else
            {
                Position = Vector3.Lerp(Position, _target, Time.deltaTime * LerpValue);
            }
        }
    }
}