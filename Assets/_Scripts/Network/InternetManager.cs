﻿using SharedData.Exchanging.DataClasses;
using SharedData.Types;
using System;
using System.Collections.Generic;
using UnityEngine;

public class InternetManager : MonoBehaviour
{
    public static InternetManager Instance { get; private set; }

    public event Action OnConnected = delegate { };

    public event Action OnConnectError = delegate { };

    public event Action<List<ArenaGameData>> OnNewGamesAvailable = delegate { };

    public event Action<ArenaGameData> OnGameUpdate = delegate { };

    public event Action<string, string, int> OnGameStart = delegate { };

    public event Action OnDisconnectFromLobby = delegate { };

    private MainServerConnector _mainServerConnector;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        InitConnectors();
    }

    private void InitConnectors()
    {
        _mainServerConnector = new MainServerConnector();
        _mainServerConnector.OnConnected += Connected;
        _mainServerConnector.OnConnectError += ConnectError;
        _mainServerConnector.OnNewGamesAvailable += NewGamesAvailable;
        _mainServerConnector.OnGameUpdate += GameUpdate;
        _mainServerConnector.OnGameStart += GameStart;
        _mainServerConnector.OnDisconnectFromLobby += DisconnectFromLobby;
    }

    private void Connected()
    {
        OnConnected();
    }

    private void ConnectError()
    {
        OnConnectError();
    }

    private void GameUpdate(ArenaGameData arenaData)
    {
        OnGameUpdate(arenaData);
    }

    private void DisconnectFromLobby()
    {
        OnDisconnectFromLobby();
    }

    private void NewGamesAvailable(List<ArenaGameData> games)
    {
        Debug.Log("Internet manager: new games received.");
        OnNewGamesAvailable(games);
    }

    private void GameStart(string code, string ip, int port)
    {
        OnGameStart(code, ip, port);
    }

    public void Login(string username, string password)
    {
        _mainServerConnector.Login(username, password);
    }

    public void RequestAvailableGames()
    {
        _mainServerConnector.Send(new AvailableGamesRequest());
    }

    public void ConnectToGame(Guid guid)
    {
        var request = new ConnectToArenaGameRequest { Id = guid };
        _mainServerConnector.Send(request);
    }

    public void DisconnectFromGame()
    {
        var request = new DisconnectFromArenaGameRequest();
        _mainServerConnector.Send(request);
    }

    public void SendReady()
    {
        _mainServerConnector.Send(new SetReadyRequest());
    }

    private void OnDestroy()
    {
        if (_mainServerConnector != null)
        {
            _mainServerConnector.Close();
        }
    }
}