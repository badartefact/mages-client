﻿using SharedData;
using SharedData.Enums;
using SharedData.Exchanging;
using SharedData.Exchanging.DataClasses;
using SharedData.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class MainServerConnector
{
    private const int Port = 34000;

    private const string Ip = "127.0.0.1";//"93.170.186.238";

    public event Action OnConnected = delegate { };

    public event Action OnConnectError = delegate { };

    public event Action<List<ArenaGameData>> OnNewGamesAvailable = delegate { };

    public event Action<ArenaGameData> OnGameUpdate = delegate { };

    public event Action<string, string, int> OnGameStart = delegate { };

    public event Action OnDisconnectFromLobby = delegate { };

    private NetConnector _netConnector;

    private BackgroundWorker _connectorToServer;
    private string _name;

    public MainServerConnector()
    {
        InitBackgroundWorkers();

        InitNetworkClient();
    }

    private void InitNetworkClient()
    {
        _netConnector = new NetConnector(Ip, Port);
        _netConnector.OnNewMessage += NewMessage;
        _netConnector.OnError += OnConnectError;
    }

    private void NewMessage(byte[] data, int protoClass)
    {
        try
        {
            var answer = (PacketClassCode)protoClass;

            Debug.Log("Received from server: " + answer);

            switch (answer)
            {
                case PacketClassCode.AvailableGames:
                    RaiseEventOnNewGames(data);
                    break;

                case PacketClassCode.UpdateGame:
                    RaiseEventOnUpdateGame(data);
                    break;

                case PacketClassCode.StartGameAnswer:
                    RaiseEventOnGameStart(data);
                    break;

                case PacketClassCode.RegisterAnswer:
                    CheckRegisterAnswer(data);
                    break;

                case PacketClassCode.DisconnectFromLobbyCommand:
                    RaiseEventDisconnectFromLobby();
                    break;

                default:
                    Debug.Log("Unhandled message: " + protoClass);
                    break;
            }
        }
        catch (Exception ex)
        {
            Debug.Log("Parse command error: " + protoClass + " " + ex.Message + " " + ex.StackTrace);
        }
    }

    private void RaiseEventDisconnectFromLobby()
    {
        OnDisconnectFromLobby();
    }

    private void CheckRegisterAnswer(byte[] data)
    {
        var registerAnswer = DataSerializer<RegisterAnswer>.Get(data);

        if (registerAnswer.CodeAccepted)
        {
            OnConnected();
        }
        else
        {
            OnConnectError();
        }
    }

    private void RaiseEventOnGameStart(byte[] data)
    {
        var answer = DataSerializer<StartGameAnswer>.Get(data);
        OnGameStart(answer.GameCode, answer.Ip, answer.Port);
    }

    private void RaiseEventOnUpdateGame(byte[] data)
    {
        var answer = DataSerializer<UpdateGameAnswer>.Get(data);
        OnGameUpdate(answer.Game);
    }

    private void RaiseEventOnNewGames(byte[] data)
    {
        var answer = DataSerializer<ArenaGamesInfoAnswer>.Get(data);
        var result = answer.Games ?? new List<ArenaGameData>();

        Debug.Log(string.Format("Available games count: {0}", result.Count));

        OnNewGamesAvailable(result);
    }

    private void InitBackgroundWorkers()
    {
        _connectorToServer = new BackgroundWorker();
        _connectorToServer.DoWork += OnBackgroundConnect;
    }

    private void OnBackgroundConnect(object sender, DoWorkEventArgs e)
    {
        _netConnector.Connect();
    }

    public void Send(PacketBase message)
    {
        _netConnector.Send(message);
    }

    public void Close()
    {
        if (_netConnector != null)
            _netConnector.Close();
    }

    public void Login(string username, string password)
    {
        _netConnector.Connect(username);
    }
}