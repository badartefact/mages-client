﻿using Lidgren.Network;
using SharedData.Exchanging;
using System;
using System.Text;
using System.Threading;
using UnityEngine;

public class NetConnector
{
    private const int IncomingDataArraySize = 10000;

    public event Action OnConnect = delegate { };

    public event Action OnError = delegate { };

    public event Action<byte[], int> OnNewMessage = delegate { };

    private const string RNotConnectedError = "NOT CONNECTED";

    private readonly string _ip;
    private readonly int _port;
    private readonly string _configName;
    private readonly bool _doLog;

    private PacketDataManager _packetManager;
    private NetClient _client;

    public string IP
    {
        get
        {
            string result = RNotConnectedError;
            if (_client.ConnectionStatus == NetConnectionStatus.Connected)
            {
                result = _client.Socket.RemoteEndPoint.ToString();
            }
            return result;
        }
    }

    public float AverageRoundtripTime
    {
        get
        {
            return _client.ServerConnection.AverageRoundtripTime;
        }
    }

    public NetConnector(string ip, int port, string configName = "MAGES", bool doLog = true)
    {
        _ip = ip;
        _port = port;
        _configName = configName;
        _doLog = doLog;

        InitPacketManager();
        InitNetworkClient();
    }

    private void InitNetworkClient()
    {
        var config = new NetPeerConfiguration(_configName);
        _client = new NetClient(config);
        var context = new SynchronizationContext();
        _client.RegisterReceivedCallback(Receive, context);
        _client.Start();
    }

    private void InitPacketManager()
    {
        _packetManager = new PacketDataManager();
        _packetManager.OnNewData += OnNewData;
        _packetManager.OnError += OnParseError;
    }

    private void OnParseError(object sender, EventArgs e)
    {
        Debug.Log("Some parse error in datapacketmanager");
    }

    private void OnNewData(object sender, byte[] data, int protoClass)
    {
        OnNewMessage(data, protoClass);
    }

    private void Receive(object state)
    {
        var message = _client.ReadMessage();

        if (_doLog) Debug.Log("Received message from server: " + message.MessageType);

        switch (message.MessageType)
        {
            case NetIncomingMessageType.Data:
                try
                {
                    var buffer = new byte[IncomingDataArraySize];

                    message.ReadBytes(buffer, 0, 2);
                    var packetSize = GetPacketLength(buffer);
                    if (_doLog) Debug.Log("Packet size: " + packetSize);
                    message.ReadBytes(buffer, 2, packetSize - 2);
                    _packetManager.Parse(buffer);
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex);
                }
                break;

            case NetIncomingMessageType.StatusChanged:
                Debug.Log(message.SenderConnection.Status);
                if (message.SenderConnection.Status == NetConnectionStatus.Connected)
                {
                    OnConnect();
                }
                else
                {
                    OnError();
                }
                break;

            default:
                Debug.Log(string.Format("Received unhandled message. Type: '{0}', Data: '{1}'", message.MessageType, Encoding.ASCII.GetString(message.Data)));
                break;
        }
    }

    private UInt16 GetPacketLength(byte[] buffer)
    {
        var dataBytes = new byte[2];
        Array.Copy(buffer, 0, dataBytes, 0, 2);
        Array.Reverse(dataBytes);
        return BitConverter.ToUInt16(dataBytes, 0);
    }

    public void Send(PacketBase packet)
    {
        if (_client.ConnectionStatus != NetConnectionStatus.Connected) return;

        Debug.Log("Going to send: " + packet.Code);

        try
        {
            byte[] result = _packetManager.GetBytes(packet);
            var message = _client.CreateMessage();
            message.Write(result);
            var sendResult = _client.SendMessage(message, NetDeliveryMethod.ReliableOrdered);
            if (sendResult != NetSendResult.Sent)
            {
                Debug.Log(string.Format("Cannot send message of type: {0}, result {1}", packet.Code, sendResult));
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message + " : " + ex.StackTrace);
        }
    }

    public void Connect(string secret = null)
    {
        try
        {
            if (string.IsNullOrEmpty(secret))
            {
                _client.Connect(_ip, _port);
            }
            else
            {
                var approval = _client.CreateMessage();
                approval.Write(secret);
                _client.Connect(_ip, _port, approval);
            }
        }
        catch (Exception ex)
        {
            Debug.Log(string.Format("Cannot connect to: {0}:{1}", _ip, _port));
            Debug.Log(ex);
            OnError();
        }
    }

    public void Close(string message = "Bye.")
    {
        try
        {
            _client.Shutdown(message);
        }
        catch { }
    }
}