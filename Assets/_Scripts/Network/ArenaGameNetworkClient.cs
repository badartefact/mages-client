﻿using SharedData;
using SharedData.Enums;
using SharedData.Exchanging;
using SharedData.Exchanging.DataClasses;
using SharedData.Types.FinishResult;
using SharedData.Types.Snapshot;
using System;
using UnityEngine;

public class ArenaGameNetworkClient
{
    #region Private members

    private readonly int _port;
    private readonly string _ip;
    private readonly string _code;

    private NetConnector _netConnector;

    #endregion Private members

    #region Public members

    public event Action<SnapshotData> OnSnapshotUpdate = delegate { };

    public event Action<FinishData> OnFinishDataReceived = delegate { };

    public float AverageRoundtripTime
    {
        get { return _netConnector.AverageRoundtripTime; }
    }

    #endregion Public members

    #region Consctructor

    public ArenaGameNetworkClient(string ip, int port, string code)
    {
        _ip = ip;
        _port = port;
        _code = code;

        InitNetworkClient();
    }

    #endregion Consctructor

    #region Initialization

    private void InitNetworkClient()
    {
        _netConnector = new NetConnector(_ip, _port, "ARENA", false);
    }

    #endregion Initialization

    #region Subscribing/unsubscribing to events

    private void SubscribeToEvents()
    {
        _netConnector.OnConnect += OnConnect;
        _netConnector.OnNewMessage += OnNewMessage;
        _netConnector.OnError += OnConnectError;
    }

    private void UnsubscribeFromEvents()
    {
        _netConnector.OnConnect -= OnConnect;
        _netConnector.OnNewMessage -= OnNewMessage;
        _netConnector.OnError -= OnConnectError;
    }

    #endregion Subscribing/unsubscribing to events

    #region Events

    private void OnConnectError()
    {
        Debug.Log("ArenaGameNetworkClient network error.");
    }

    private void OnNewMessage(byte[] data, int protoClass)
    {
        try
        {
            var answer = (PacketClassCode)protoClass;
            switch (answer)
            {
                case PacketClassCode.UpdateSnapshot:
                    var snapshotPacket = DataSerializer<UpdateSnapshotAnswer>.Get(data);
                    OnSnapshotUpdate(snapshotPacket.Snapshot);
                    break;

                case PacketClassCode.FinishData:
                    var finishDataPacket = DataSerializer<FinishDataAnswer>.Get(data);
                    OnFinishDataReceived(finishDataPacket.FinishData);
                    break;

                default:
                    Debug.Log("Unhandled message: " + protoClass);
                    break;
            }
        }
        catch (Exception ex)
        {
            Debug.Log("Parse command error: " + protoClass + " " + ex.Message + " " + ex.StackTrace);
        }
    }

    private void OnConnect()
    {
        Debug.Log("Connected to game server.");
    }

    #endregion Events

    #region Public methods

    public void Start()
    {
        SubscribeToEvents();
        _netConnector.Connect(_code);
    }

    public void Stop()
    {
        UnsubscribeFromEvents();
        _netConnector.Close();
    }

    public void Send(PacketBase command)
    {
        _netConnector.Send(command);
    }

    #endregion Public methods
}