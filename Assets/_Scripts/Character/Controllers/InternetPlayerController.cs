using Assets._Scripts.Arena;
using GameServer.Configuration;
using SharedData.Enums;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Physics = UnityEngine.Physics;

public class InternetPlayerController : MonoBehaviour
{
    private BattleArenaController _arenaController;

    private int _layerMask;
    private readonly int _groundLayer = GlobalSettings.GroundLayer;
    private readonly int _lavaLayer = GlobalSettings.LavaLayer;

    private SortedList<KeyCode, SpellName> _spellHotKeys;
    private bool _spellIsSelected;
    private SpellName _selectedSpell;
    private ConfigurationManager _configurationManager;

    private void Start()
    {
        _arenaController = GameObject.Find("ArenaManager").GetComponent<BattleArenaController>();
        _layerMask = _groundLayer | _lavaLayer;

        _configurationManager = ConfigurationManager.Instance;
        InitHotkeys();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        for (int i = 0; i < _spellHotKeys.Count; i++)
        {
            if (Input.GetKeyDown(_spellHotKeys.Keys[i]))
            {
                _spellIsSelected = true;
                _selectedSpell = _spellHotKeys.Values[i];

                break;
            }
        }

        if (_spellIsSelected)
        {
            var spellData = _configurationManager.SpellsDescriptions[_selectedSpell];
            if (!spellData.NeedTargeting)
            {
                CastSpell();
            }
            else if (Input.GetButtonDown("Fire1"))
            {
                CastSpell();
            }
        }

        CheckMovement();
    }

    private void CastSpell()
    {
        Debug.Log("Casting spell: " + _selectedSpell);
        _spellIsSelected = false;
        var target = GetMousePosition();
        _arenaController.CastSpell(_selectedSpell, target);
    }

    private void CheckMovement()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            if (GroundIsHited())
            {
                _spellIsSelected = false;
                var target = GetMousePosition();
                _arenaController.MoveTo(target);
            }
        }
    }

    private void InitHotkeys()
    {
        _spellHotKeys = new SortedList<KeyCode, SpellName>
        {
            {KeyCode.G, SpellName.Fireball},
            {KeyCode.F, SpellName.Explode},
            {KeyCode.D, SpellName.Lighting},
            {KeyCode.R, SpellName.Teleport}
        };
    }

    private Vector3 GetMousePosition()
    {
        var result = Vector3.zero;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, _layerMask))
        {
            result = hit.point;
        }
        return result;
    }

    private bool GroundIsHited()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, _layerMask))
        {
            return true;
        }
        return false;
    }
}
