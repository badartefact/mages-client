﻿using System.Collections.Generic;

public class SafeQueue<T>
{
    private object _syncObject;
    private Queue<T> _queue;

    public int Count
    {
        get
        {
            return _queue.Count;
        }
    }

    public SafeQueue()
    {
        _syncObject = new object();
        _queue = new Queue<T>();
    }

    public void Enqueue(T item)
    {
        lock (_syncObject)
        {
            _queue.Enqueue(item);
        }
    }

    public T Dequeue()
    {
        lock (_syncObject)
        {
            return _queue.Dequeue();
        }
    }
}