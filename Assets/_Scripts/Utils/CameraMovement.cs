﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private const float CameraSpeed = 40;
    private const float MoveOffset = 5;
    private bool _movingCameraWithWheel;
    private Vector2 _oldMousePosition = Vector2.zero;

    private void Start()
    {
        _oldMousePosition = Vector2.zero;
    }

    private void Update()
    {
        CheckKeyboard();
        //CheckMouse();
        CheckMouseWheel();
        if (_movingCameraWithWheel)
        {
            MoveCameraWithWheel();
        }
    }

    private void MoveCameraWithWheel()
    {
        var currentMousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        var xMovement = _oldMousePosition.x - currentMousePosition.x;
        var zMovement = _oldMousePosition.y - currentMousePosition.y;
        _oldMousePosition = currentMousePosition;

        if (xMovement != 0.0f || zMovement != 0.0f)
        {
            var zSpeed = zMovement * GlobalSettings.CameraZHorizontalSpeed * Time.deltaTime;
            var xSpeed = xMovement * GlobalSettings.CameraXHorizontalSpeed * Time.deltaTime;

            transform.position = new Vector3(transform.position.x + xSpeed, transform.position.y, transform.position.z + zSpeed);
        }
    }

    private void CheckMouseWheel()
    {
        if (Input.GetMouseButtonDown(2))
        {
            _oldMousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            _movingCameraWithWheel = true;
        }

        var ySize = GlobalSettings.CameraYVeticalSpeed;
        var zSize = -GlobalSettings.CameraZVerticalSpeed;
        var mouseWheelValue = Input.GetAxis("Mouse ScrollWheel");

        if (Input.GetMouseButtonUp(2))
        {
            _movingCameraWithWheel = false;
        }
        if (mouseWheelValue > 0) // down
        {
            var position = Camera.main.transform.position;
            if (position.y < ySize)
            {
                return;
            }
            transform.position = new Vector3(position.x, position.y - ySize, position.z - zSize);
        }
        if (mouseWheelValue < 0) // up
        {
            Vector3 position = Camera.main.transform.position;
            transform.position = new Vector3(position.x, position.y + ySize, position.z + zSize);
        }
    }

    private void CheckKeyboard()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            MoveCameraLeft();
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            MoveCameraRight();
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            MoveCameraUp();
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            MoveCameraDown();
        }
    }

    private void CheckMouse()
    {
        var rightDiff = Mathf.Abs(Screen.width - Input.mousePosition.x);

        if (rightDiff < MoveOffset)
        {
            MoveCameraRight();
        }

        if (Input.mousePosition.x < MoveOffset)
        {
            MoveCameraLeft();
        }

        float bottomDiff = Mathf.Abs(Screen.height - Input.mousePosition.y);

        if (bottomDiff < MoveOffset)
        {
            MoveCameraUp();
        }

        if (Input.mousePosition.y < MoveOffset)
        {
            MoveCameraDown();
        }
    }

    private void MoveCameraLeft()
    {
        transform.position += CameraSpeed * Vector3.left * Time.deltaTime;
    }

    private void MoveCameraRight()
    {
        transform.position += CameraSpeed * Vector3.right * Time.deltaTime;
    }

    private void MoveCameraUp()
    {
        transform.position += CameraSpeed * Vector3.forward * Time.deltaTime;
    }

    private void MoveCameraDown()
    {
        transform.position += CameraSpeed * Vector3.back * Time.deltaTime;
    }
}