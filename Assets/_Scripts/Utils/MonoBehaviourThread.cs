﻿using System;
using UnityEngine;

public class MonoBehaviourThread : MonoBehaviour
{
    private readonly SafeQueue<Action> _actions;

    public MonoBehaviourThread()
    {
        _actions = new SafeQueue<Action>();
    }

    private void Update()
    {
        while (_actions.Count > 0)
        {
            _actions.Dequeue()();
        }

        OnUpdate();
    }

    protected virtual void OnUpdate()
    {
    }

    protected void RunInMainThread(Action action)
    {
        _actions.Enqueue(action);
    }
}